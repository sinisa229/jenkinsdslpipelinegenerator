job {
    name 'Commit'
	logRotator(-1, 7, -1, -1)
	scm {
		git ('https://bitbucket.org/sinisa229/jenkinsdslpipelinegenerator.git')
	}
	jdk('JDK_7u25')
	triggers {
		scm('* * * * *')
	}
	steps {
        maven('clean package')
    }
	
	publishers {
        archiveJunit("glob", null)
		downstreamParameterized {
			trigger('CodeCoverage') {
				predefinedProp('svnCommitNumber', '${SVN_REVISION}')
			}
		}
    }
}


job {
    name 'CodeCoverage'
}
